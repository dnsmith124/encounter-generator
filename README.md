# README #

### What is this repository for? ###

* This repository holds the source code as well as an executable .jar file for a Dungeons and Dragons Encounter Generator.


* Version 1.0
### Set up and use: ###
* 	Download repo and run DnD_Encounter_Gen.jar in the dist folder.
	
	
* 	The program allows for the creation of unique enemies to be used in DnD encounters. Fill in the relevant fields (Be sure to input integers into the HP and AC fields)
* and click the "Add Enemy" button to add an enemy to the list. Any number of enemies may then be selected in the list (hold down the ctrl key to select multiple enemies). 

* 	When the desired enemies are selected from the list, click the "Create Encounter" button. A box will appear for each enemy that was selected, prompting 
* the user to select the desired number of each particular enemy. After the desired number of each enemy type has been selected, a window will appear allowing
* the user to name the encounter. Click the "OK" button to save the encounter.

*	A created encounter can be selected from the Encounter List. Once the desired encounter has been selected, click the "Run Encounter" button to display
* the relevant statistics for the enemies that are in the selected encounter. 


* 	The program saves both Enemy and Encounter objects to "masterEnemySet.ser" and "masterEncounterSet.jar", respectively. The application saves all 
* data whenever an action is performed. Though a user can simply close out the application without fear of losing any data, there is also a
* "Save and Exit" button in the bottom right corner. 

### Who do I talk to? ###

d_smith124@hotmail.com
