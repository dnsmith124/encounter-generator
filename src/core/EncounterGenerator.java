package core;

import java.util.Collections;
import java.util.TreeSet;

public class EncounterGenerator {
//	public static void main(String[] args) {
//		//create example enemies
//		Enemy rat1 = new Enemy("Rat1", "Beast", "Bite: 1d4", 4, 10);
//		Enemy rat2 = new Enemy("Rat2", "Beast", "Bite: 1d4", 4, 10);
//		Enemy rat3 = new Enemy("Rat3", "Beast", "Bite: 1d4", 4, 10);
//
//		//create example set of enemies
//		TreeSet<Enemy> set1 = new TreeSet<>();
//		Collections.addAll(set1, rat1, rat2, rat3);
//
//		//create encounter based on example list1
//		Encounter ratEncounter1 = new Encounter("Rat Encounter 1", set1);
//
//		//create another example set of enemies
//		TreeSet<Enemy> set2 = new TreeSet<>();
//		Collections.addAll(set2, rat1, rat2, rat3);
//		//create encounter based on example list2
//		Encounter ratEncounter2 = new Encounter("Rat Encounter 2", set2);
//
//		//access master enemySet and set to all current enemies(set1)
//		EnemySet.setEnemySet(set1);
//		
//		//adding individual enemy to EnemySet
//		Enemy gnoll = new Enemy("Lesser Gnoll", "Humanoid", "Broken Sword 1d4+1", 12, 12);
//		EnemySet.addEnemy(gnoll);
//		
//		//add all current encounters to set3
//		TreeSet<Encounter> set3 = new TreeSet<>();
//		Collections.addAll(set3, ratEncounter1, ratEncounter2);
//		//access master encounterSet and set to all current encounters(set3)
//		EncounterSet.setEncounterSet(set3);
//		
//		//adding individual encounter to EncounterSet
//		TreeSet<Enemy> set4 = new TreeSet<>();
//		Collections.addAll(set4, rat1, rat3, gnoll);
//		Encounter encounter1 = new Encounter("Rats with Gnoll Leader", set4);
//		EncounterSet.addEncounter(encounter1);
//
//		//print master enemySet and encounterSet
//		System.out.println(EnemySet.getEnemySet());
//		System.out.println(EncounterSet.getEncounterSet());
//		
//		//saving total current enemies to masterEnemyList.ser
//		EnemySet.saveEnemySet();
//		//reading masterEnemyList.ser and setting it to a new variable
//		TreeSet<Enemy> enemySet2 = EnemySet.readEnemySet();
//		//printing said variable to console
//		System.out.println(enemySet2);
//		
//		//saving total current encounters to masterEncounterList.ser
//		EncounterSet.saveEncounterSet();
//		//reading masterEncounterList.ser and setting it to a new variable
//		TreeSet<Encounter> encounterSet2 = EncounterSet.readEncounterSet();
//		//printing said variable to console
//		System.out.println(encounterSet2);
//
//	}
}
