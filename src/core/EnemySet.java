package core;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.TreeSet;

public class EnemySet implements Serializable {
	private static EnemySet instance;
	private static TreeSet<Enemy> enemySet = EnemySet.readEnemySet();
	
	private static final String saveLocation = "masterEnemySet.ser";

	private EnemySet() {

	}

	public static synchronized EnemySet getInstance() {
		if (instance == null) {
			instance = new EnemySet();
		}
		return instance;
	}

	public static TreeSet<Enemy> getEnemySet() {
		return enemySet;
	}

	public static void setEnemySet(TreeSet<Enemy> enemySet) {
		EnemySet.enemySet = enemySet;
		System.out.println("Setting master enemy list...");
	}

	public static void addEnemy(Enemy enemy) {
		EnemySet.enemySet.add(enemy);
	}

	public static void removeEnemy(Enemy enemy) {
		enemySet.remove(enemy);
	}
	public static void saveEnemySet() {
		System.out.println("Writing enemies to " + saveLocation);

		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(saveLocation));) {
			for (Enemy enemy : EnemySet.getEnemySet()) {
				oos.writeObject(enemy);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static TreeSet<Enemy> readEnemySet() {
		System.out.println("Reading enemies...");
		TreeSet<Enemy> masterEnemySet = new TreeSet<>();
                
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(saveLocation))) {
			while (true) {
				Enemy enemy = (Enemy) ois.readObject();
				 masterEnemySet.add(enemy);
			}
		} catch (EOFException e) {
			return masterEnemySet;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
}
