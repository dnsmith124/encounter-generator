package core;

import java.io.Serializable;
import java.util.TreeSet;

public class Encounter implements Comparable<Encounter>, Serializable {
	private String name;
	private static int count = 1;
	private int id;
	private TreeSet<Enemy> encounterEnemies;

	public Encounter() {
		this.id = count++;
	}

	public Encounter(String name, TreeSet<Enemy> encounterEnemies) {
		this.name = name;
		this.id = count++;
		this.encounterEnemies = encounterEnemies;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public TreeSet<Enemy> getEncounterEnemies() {
		return encounterEnemies;
	}

	public void setEncounterEnemies(TreeSet<Enemy> encounterEnemies) {
		this.encounterEnemies = encounterEnemies;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int compareTo(Encounter e) {
		return this.name.compareTo(e.name);
	}
}
