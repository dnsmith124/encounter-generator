package core;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.TreeSet;

public class EncounterSet implements Serializable {
	private static EncounterSet instance;
	private static TreeSet<Encounter> encounterSet = EncounterSet.readEncounterSet();

	private static final String saveLocation = "masterEncounterSet.ser";

	private EncounterSet() {

	}

	public static synchronized EncounterSet getInstance() {
		if (instance == null) {
			instance = new EncounterSet();
		}
		return instance;
	}

	public static TreeSet<Encounter> getEncounterSet() {
		return encounterSet;
	}

	public static void setEncounterSet(TreeSet<Encounter> encounterSet) {
		EncounterSet.encounterSet = encounterSet;
		System.out.println("Setting master encounter list...");
	}

	public static void addEncounter(Encounter encounter) {
		EncounterSet.encounterSet.add(encounter);
	}

	public static void removeEncounter(Encounter encounter) {
		EncounterSet.encounterSet.remove(encounter);
	}

	public static void saveEncounterSet() {
		System.out.println("Writing encounters to " + saveLocation);

		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(saveLocation));) {
                    
			for (Encounter encounter : EncounterSet.getEncounterSet()) {
				oos.writeObject(encounter);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static TreeSet<Encounter> readEncounterSet() {
		System.out.println("Reading encounters...");
		TreeSet<Encounter> masterEncounterSet = new TreeSet<>();

		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(saveLocation))) {
			while (true) {
				Encounter encounter = (Encounter) ois.readObject();
				masterEncounterSet.add(encounter);
			}
		} catch (EOFException e) {
			return masterEncounterSet;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

}
