package core;

import java.io.Serializable;

public class Enemy implements Comparable<Enemy>, Serializable {
	private String name;
	private String type;
	private String attack;
	private static int count = 1;
	private int id;
	private int hp;
	private int ac;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAttack() {
		return attack;
	}

	public void setAttack(String attack) {
		this.attack = attack;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public int getAc() {
		return ac;
	}

	public void setAc(int ac) {
		this.ac = ac;
	}

	public Enemy(String name, String type, String attack, int hp, int ac) {
		this.name = name;
		this.type = type;
		this.attack = attack;
		this.id = count++;
		this.hp = hp;
		this.ac = ac;
	}
        
        /*        public String blahBlah() {
        return name + " " + attack + " " + hp + " " + ac;
        }*/

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Enemy other = (Enemy) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public int compareTo(Enemy e) {
		return this.name.compareTo(e.name);
	}
}